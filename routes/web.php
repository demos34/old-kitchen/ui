<?php

use App\Http\Controllers\Consult\ConsultController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Home\PostsController;
use App\Http\Controllers\Partners\PartnersController;
use App\Http\Controllers\Recipes\RecipesController;
use App\Http\Controllers\Recipes\TypesController;
use App\Http\Controllers\Book\BooksController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['analyze', 'count']], function() {
    // all routes is under the analyze middleware - software to analyze the movement in the site


    Route::get('/', function () {
        return view('welcome');
    });

//Auth::routes();

//home controller

    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/', [HomeController::class, 'index'])->name('index-home');
    Route::get('/contacts', [HomeController::class, 'contacts'])->name('contacts');
    Route::put('/contacts', [HomeController::class, 'sendUsMessage'])->name('contacts-post');
    Route::get('/security', [HomeController::class, 'security'])->name('security');
    Route::get('/security', [HomeController::class, 'security'])->name('security');
    Route::get('/terms', [HomeController::class, 'terms'])->name('terms');
    Route::get('/terms', [HomeController::class, 'terms'])->name('terms');
    Route::get('/cookies', [HomeController::class, 'cookies'])->name('cookies');
    Route::post('/search', [HomeController::class, 'search'])->name('search');

//recipes controller

    Route::get('/recipes', [RecipesController::class, 'index'])->name('recipes');
    Route::get('/recipes/{recipe}', [RecipesController::class, 'show'])->name('recipes-show');


//types controller

    Route::get('/types/{type}', [TypesController::class, 'show'])->name('types-show');
    Route::get('/types', [TypesController::class, 'index'])->name('types-index');
    Route::get('/types/first/{type}/{letter}', [TypesController::class, 'showByFirstLetter'])->name('types-letter');

//books controller

    Route::get('/books', [BooksController::class, 'index'])->name('books-index');
    Route::get('/books/{book}', [BooksController::class, 'show'])->name('books-show');
    Route::get('/books/get/{book}', [BooksController::class, 'getOne'])->name('books-get-one');
    Route::put('/books/get/{book}', [BooksController::class, 'getOneSend'])->name('books-get-one-post');

//partners
    Route::get('/partners', [PartnersController::class, 'index'])->name('partners-index');
    Route::get('/partners/{partner}', [PartnersController::class, 'show'])->name('partners-show');

//blog

    Route::get('/posts', [PostsController::class, 'index'])->name('posts-index');
    Route::get('/posts/{post}', [PostsController::class, 'show'])->name('posts-show');

//youtube

    Route::get('/tube', [PostsController::class, 'tube'])->name('tube-index');
    Route::get('/tube/{tube}', [PostsController::class, 'tubeShow'])->name('tube-show');

//consults


    Route::view('/consult-off', 'home.consultations.activity')->name('activity-off');
    Route::group(['middleware' => 'isConsultOn'], function () {
        Route::get('/consultations', [ConsultController::class, 'index'])
            ->name('consult-index');

        Route::get('/consultations/{type}', [ConsultController::class, 'show'])
            ->name('consult-show');
        Route::post('/consultations/{consultType}', [ConsultController::class, 'store'])
            ->name('consult-form-show-post');
        Route::patch('/consultations/{consultType}', [ConsultController::class, 'storeComment'])
            ->name('consult-store-comment');
    });

});
