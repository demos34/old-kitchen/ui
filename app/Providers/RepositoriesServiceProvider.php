<?php

namespace App\Providers;

use App\Repositories\BooksRepository;
use App\Repositories\BooksRepositoryInterface;
use App\Repositories\ConsultRepository;
use App\Repositories\ConsultRepositoryInterface;
use App\Repositories\HomeInfoRepository;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\PostsRepository;
use App\Repositories\PostsRepositoryInterface;
use App\Repositories\RecipesRepository;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepository;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepository;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(BooksRepositoryInterface::class, BooksRepository::class);
        $this->app->singleton(ConsultRepositoryInterface::class, ConsultRepository::class);
        $this->app->singleton(HomeInfoRepositoryInterface::class, HomeInfoRepository::class);
        $this->app->singleton(PostsRepositoryInterface::class, PostsRepository::class);
        $this->app->singleton(RecipesRepositoryInterface::class, RecipesRepository::class);
        $this->app->singleton(TrafficRepositoryInterface::class, TrafficRepository::class);
        $this->app->singleton(TypesRepositoryInterface::class, TypesRepository::class);
    }
}
