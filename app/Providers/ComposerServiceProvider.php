<?php

namespace App\Providers;

use App\Models\Consult\ConsultActivity;
use App\Models\Consult\ConsultTypesStatus;
use App\Models\Home\HomeInfo;
use App\Models\Type\Type;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.navbar', function ($view) {
            $view->with
            ([
                'types' => Type::all(),
                'homeInfo' => HomeInfo::find(1),
                'on' => ConsultActivity::firstOrFail(),
                'beginnerStatus' => ConsultTypesStatus::where('consult_type_id', 1)->first(),
                'businessStatus' => ConsultTypesStatus::where('consult_type_id', 2)->first(),

            ]);
        });
    }
}
