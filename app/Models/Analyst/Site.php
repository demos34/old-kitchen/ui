<?php

namespace App\Models\Analyst;

use App\Models\Count\Count;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Site extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function analysts(): HasMany
    {
        return $this->hasMany(Analyst::class);
    }

    public function counts(): HasMany
    {
        return $this->hasMany(Count::class);
    }
}
