<?php

namespace App\Models\Type;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipes\Recipe','recipes_types', 'type_id', 'recipe_id');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Recipes\Image');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tag\Keyword');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
