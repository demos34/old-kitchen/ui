<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultActivity extends Model
{
    use HasFactory;
}
