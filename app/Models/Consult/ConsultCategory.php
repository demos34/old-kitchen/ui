<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultCategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function consultSubcategories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ConsultSubcategory::class);
    }

    public function clients(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Client::class);
    }

    public function clientCategoryAntherDishes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ClientCategoryAntherDish::class);
    }
}
