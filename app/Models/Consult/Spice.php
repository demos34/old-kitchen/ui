<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spice extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function clientInformation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ClientInformation::class);
    }
}
