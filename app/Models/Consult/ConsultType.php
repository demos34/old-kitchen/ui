<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultType extends Model
{
    use HasFactory;

    public function consultServices(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ConsultService::class);
    }

    public function client(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Client::class);
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function consultTypesStatus(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultTypesStatus::class);
    }

    public function description(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultDescription::class);
    }
}
