<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultComment extends Model
{
    use HasFactory;

    protected $guarded = [];
}
