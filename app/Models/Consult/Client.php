<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function clientInformation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ClientInformation::class);
    }

    public function consultSubcategories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ConsultSubcategory::class);
    }

    public function consultCategories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ConsultCategory::class);
    }

    public function clientCategoryAntherDishes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ClientCategoryAntherDish::class);
    }

    public function message(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultMessage::class);
    }

    public function consultStatuses(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ConsultStatus::class);
    }

    public function consultType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConsultType::class);
    }

    public function businessMessage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(BusinessMessage::class);
    }


}
