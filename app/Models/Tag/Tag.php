<?php

namespace App\Models\Tag;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    public function types()
    {
        return $this->belongsToMany('App\Models\Type\Type');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipes\Recipe');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Home\Post');
    }

    public function tubes()
    {
        return $this->belongsToMany('App\Models\Home\Tube');
    }
}
