<?php

namespace App\Models\Count;

use App\Models\Analyst\Site;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Count extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function countType(): BelongsTo
    {
        return $this->belongsTo(CountType::class);
    }

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
}
