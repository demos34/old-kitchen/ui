<?php

namespace App\Models\Count;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CountType extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function counts(): HasMany
    {
        return $this->hasMany(Count::class);
    }

    public function countables(): HasMany
    {
        return $this->hasMany(Countable::class);
    }
}
