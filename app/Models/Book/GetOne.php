<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GetOne extends Model
{
    use HasFactory;

    protected $guarded = [];
}
