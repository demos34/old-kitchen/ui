<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public function buyFrom()
    {
        return $this->belongsToMany('App\Models\Book\BuyFrom', 'buying_books', 'book_id', 'from_where_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
