<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function messages()
    {
        return $this->belongsToMany('App\Models\Book\Message');
    }
}
