<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyFrom extends Model
{
    use HasFactory;

    public function books()
    {
        return $this->belongsToMany('App\Models\Book\Book', 'buying_books', 'from_where_id', 'book_id');
    }
}
