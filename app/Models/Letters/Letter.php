<?php

namespace App\Models\Letters;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'letter';
    }
}
