<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tube extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tag\Keyword');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
