<?php

namespace App\Models\Recipes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function recipe()
    {
        return $this->belongsTo('App\Models\Recipes\Recipe');
    }
}
