<?php

namespace App\Models\Recipes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipes\Recipe', 'recipes_images', 'img_id', 'recip_id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type\Type');
    }
}
