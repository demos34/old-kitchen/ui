<?php

namespace App\Models\Recipes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    public function types()
    {
        return $this->belongsToMany('App\Models\Type\Type', 'recipes_types', 'recipe_id', 'type_id');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Recipes\Image', 'recipes_images', 'recip_id', 'img_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Recipes\Product', 'recip_id');
    }

    public function draftRecipe()
    {
        return $this->belongsTo('App\Models\Recipes\DraftRecipe');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tag\Keyword');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
