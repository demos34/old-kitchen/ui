<?php

namespace App\Models\Recipes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeToCook extends Model
{
    use HasFactory;

    public function draftRecipe()
    {
        return $this->belongsTo('App\Models\Recipes\DraftRecipe');
    }
}
