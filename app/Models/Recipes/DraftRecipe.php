<?php

namespace App\Models\Recipes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DraftRecipe extends Model
{
    use HasFactory;

    public function timeToCook()
    {
        return $this->hasOne('App\Models\Recipes\TimeToCook');
    }

    public function recipe()
    {
        return $this->hasOne('App\Models\Recipes\Recipe');
    }
}
