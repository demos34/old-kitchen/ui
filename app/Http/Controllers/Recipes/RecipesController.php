<?php

namespace App\Http\Controllers\Recipes;

use App\Http\Controllers\Controller;
use App\Models\Recipes\DraftRecipe;
use App\Models\Recipes\Recipe;
use App\Models\Recipes\TimeToCook;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Http\Request;

class RecipesController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TypesRepositoryInterface
     */
    private $typesRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                TypesRepositoryInterface $typesRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->typesRepository = $typesRepository;
        $this->recipesRepository = $recipesRepository;
    }

    public function index()
    {
        $action = 'ui-index';
//        $this->trafficRepository->getRemoteAddress($action);

        $allRecipes = $this->recipesRepository->getAllRecipes();

        return view('recipes.recipes.index')->with(
            [
                'recipes' => $allRecipes,
            ]
        );

    }

    public function show(Recipe $recipe)
    {
        $action = 'ui-show';
//        $this->trafficRepository->getRemoteAddress($action);

        $type = $this->recipesRepository->getType($recipe);
        $timeToCook = $this->recipesRepository->getTimeToCook($recipe->draft_id);

        return view('recipes.recipes.show')->with(
            [
                'recipe' => $recipe,
                'type' => $type,
                'time' => $timeToCook,
            ]
        );
    }
}
