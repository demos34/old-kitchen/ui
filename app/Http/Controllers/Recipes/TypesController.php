<?php

namespace App\Http\Controllers\Recipes;

use App\Http\Controllers\Controller;
use App\Models\Letters\Letter;
use App\Models\Type\Type;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Http\Request;
use Ramsey\Uuid\Type\TypeInterface;

class TypesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TypesRepositoryInterface
     */
    private $typesRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                TypesRepositoryInterface $typesRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->typesRepository = $typesRepository;
        $this->recipesRepository = $recipesRepository;
    }

    public function index(Type $type)
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);
        dd($type);
    }

    public function show(Type $type)
    {
        $action = 'ui-show';
        $this->trafficRepository->getRemoteAddress($action);


        $letters = $this->typesRepository->getLetters();
        $count = 1;

        return view('recipes.types.show')->with(
            [
                'type' => $type,
                'letters' => $letters,
                'count' => $count,
            ]
        );
    }

    public function showByFirstLetter(Type $type, Letter $letter)
    {
        $action = 'ui-show-by-first-letter';
        $this->trafficRepository->getRemoteAddress($action);

        $recipes = $this->typesRepository->getRecipesByFirstLetter($type, $letter);
        return view('recipes.types.letter')->with(
            [
                'recipes' => $recipes,
                'type' => $type,
                'letter' => $letter,
            ]
            );
    }
}
