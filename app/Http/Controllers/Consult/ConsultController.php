<?php

namespace App\Http\Controllers\Consult;

use App\Http\Controllers\Controller;
use App\Models\Consult\ConsultActivity;
use App\Models\Consult\ConsultCategory;
use App\Models\Consult\ConsultComment;
use App\Models\Consult\ConsultService;
use App\Models\Consult\ConsultType;
use App\Models\Consult\ConsultTypesStatus;
use App\Models\Consult\Spice;
use App\Models\Recipes\Recipe;
use App\Repositories\ConsultRepositoryInterface;
use Illuminate\Http\Request;

class ConsultController extends Controller
{
    /**
     * @var ConsultRepositoryInterface
     */
    private $consultRepository;

    public function __construct(ConsultRepositoryInterface $consultRepository)
    {
        $this->consultRepository = $consultRepository;
    }

    public function index()
    {
        return view('home.consultations.index');
    }

    public function show($type)
    {
        if($type !== 'beginner' && $type !== 'business') return back();

        $consultType = $this->consultRepository->getType($type);

        self::isNotAllowed($consultType);


        $me = session()->getId();
        $upRecipes = $this->consultRepository->getRecipes();
        $downRecipes = $this->consultRepository->getRecipes();
        $services = $this->consultRepository->getServices($consultType);
        $categories = $this->consultRepository->getCategories();
        $spices = $this->consultRepository->getSpices();
        $comments = $this->consultRepository->getAllComments($consultType);

        return view('home.consultations.show')->with(
            [
                'type' => $type,
                'upRecipes' => $upRecipes,
                'downRecipes' => $downRecipes,
                'services' => $services,
                'categories' => $categories,
                'spices' => $spices,
                'comments' => $comments,
                'me' => $me,
                'consultType' => $consultType,
            ]
        );
    }

    public function store(Request $request, ConsultType $consultType)
    {

        self::isNotAllowed($consultType);
        if (!$this->consultRepository->store($request, $consultType)) return self::returnBack(['session' => 'warning', 'message' => 'Трябва да изчакате 30 минути преди да въведете отново!',]);

        return self::returnBack(['session' => 'success', 'message' => 'Заявката Ви е изпратена успешно!',]);

    }

    public function storeComment(Request $request, ConsultType $consultType): \Illuminate\Http\RedirectResponse
    {

        self::isNotAllowed($consultType);
        $msg = $this->consultRepository->storeComment($request, $consultType);
        return self::returnBack($msg);
    }

    protected static function returnBack($msg): \Illuminate\Http\RedirectResponse
    {
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    protected static function isNotAllowed(ConsultType $consultType)
    {
        $status = ConsultTypesStatus::where('consult_type_id',  $consultType->id)->first();
        if($consultType->id === 1){
            $msg = "личните консултации";
        } else {
            $msg = "бизнес консултациите";
        }
        if($status->is_on == false) {
            abort(403, 'В момента ' . $msg . ' не са активни');
        }
    }
}
