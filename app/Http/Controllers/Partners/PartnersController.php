<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use App\Models\Partners\Partner;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, HomeInfoRepositoryInterface $homeInfoRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
    }

    public function index()
    {
        $action = 'ui index';
        $this->trafficRepository->getRemoteAddress($action);

        $partners = $this->homeInfoRepository->getAllPartners();

        return view('partners.index')->with(
            [
                'partners' => $partners,
            ]
        );
    }

    public function show(Partner $partner)
    {
        $action = 'ui show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('partners.show')->with(
            [
                'partner' => $partner,
            ]
        );
    }
}
