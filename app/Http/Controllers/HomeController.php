<?php

namespace App\Http\Controllers;

use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;


    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                HomeInfoRepositoryInterface $homeInfoRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);
        return view('home');
    }
}
