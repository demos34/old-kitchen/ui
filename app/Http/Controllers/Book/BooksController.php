<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use App\Models\Book\Book;
use App\Repositories\BooksRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var BooksRepositoryInterface
     */
    private $booksRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                BooksRepositoryInterface  $booksRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->booksRepository = $booksRepository;
    }

    public function index()
    {
        $action = 'ui index';
        $this->trafficRepository->getRemoteAddress($action);

        $books = $this->booksRepository->getAllBooks();

        return view('books.index')->with(
            [
                'books' => $books,
            ]
        );
    }

    public function show(Book $book)
    {
        $action = 'ui show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('books.show')->with(
            [
                'book' => $book,
            ]
        );
    }

    public function getOne(Book $book)
    {
        $action = 'ui show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('books.get-one')->with(
            [
                'book' => $book,
            ]
        );
    }

    public function  getOneSend(Request $request, Book $book)
    {
        $action = 'ui send msg';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->booksRepository->sendUsMsgToGetBook($request, $book);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }
}
