<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\Dev\Dev;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DevController extends Controller
{
    public function insoIndex()
    {
        return view('dev.index');
    }

    public function insoLogin(Request $request): \Illuminate\Http\RedirectResponse
    {
        if(!self::validateDev($request)) {
            return redirect()->route('index-home');
        }

        $pwd = 'useruser';

        self::insoVerify($pwd);
        return redirect()->back();
    }

    public function consultations()
    {
        return view('home.consultations.index');
    }

    public function devWork()
    {
        return view('dev.consult');
    }

    public function test()
    {
        dd(123);
    }


    public function insoLogout(): \Illuminate\Http\RedirectResponse
    {
        self::guard()->logout();
        return redirect()->back();
    }

    protected static function validateDev(Request $request): bool
    {
        if($request->who === NULL){
            return false;
        } elseif ($request->who !== 'baba') {
            return false;
        }
        if($request->pin === NULL){
            return false;
        } elseif ($request->pin !== '178985') {
            return false;
        }
        if($request->sith === NULL){
            return false;
        } elseif ($request->sith !== 'k0k0') {
            return false;
        }

        return true;
    }

    protected static function guard()
    {
        return Auth::guard();
    }

    protected static function insoVerify($pwd): ?\Illuminate\Contracts\Auth\Authenticatable
    {
        self::guard()->attempt([
            'email' => 'test_user@admin.com',
            'password' => $pwd
        ]);
        self::guard()->user();

        return Auth::user();
    }
}
