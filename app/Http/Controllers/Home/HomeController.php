<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Home\HomeInfo;
use App\Models\Home\IndexTag;
use App\Models\Recipes\Recipe;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                HomeInfoRepositoryInterface $homeInfoRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
        $this->recipesRepository = $recipesRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        $homeInfo = $this->homeInfoRepository->getHomeInfo();
        $tags = $this->homeInfoRepository->getTags();
        $lastTag = $this->homeInfoRepository->getLastTag();

        $recipesCount = $homeInfo->recipes_count;
        $lastNRecipes = $this->recipesRepository->getLastNRecipes($recipesCount);

        return view('home.index')->with(
            [
                'homeInfo' => $homeInfo,
                'tags' => $tags,
                'lastTag' => $lastTag,
                'recipes' => $lastNRecipes,
            ]
        );
    }

    public function contacts()
    {
        $action = 'ui-contacts';
        $this->trafficRepository->getRemoteAddress($action);

        return view('home.contacts');
    }

    public function sendUsMessage(Request $request)
    {
        $action = 'ui-send-us-msg';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->homeInfoRepository->contacts($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function security()
    {
        return view('policy.security');
    }

    public function terms()
    {
        return view('policy.summary');
    }

    public function cookies()
    {
        return view('policy.cookies');
    }

    public function search(Request $request)
    {
        if (!isset($request->search)) {
            return redirect()->back();
        }
        if ($request->search === NULL) {
            return redirect()->back();
        }
        $validated = $request->validate(
            [
                'search' => 'required|min:1|max:50',
            ]
        );
        $searchedRecipes = Recipe::where('title', 'LIKE', '%'. $validated['search'] .'%')->orderByDesc('updated_at')->paginate(20);
        return view('recipes.recipes.index')->with(
            [
                'recipes' => $searchedRecipes,
            ]
        );
    }

}
