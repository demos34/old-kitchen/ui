<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Home\Post;
use App\Models\Home\Tube;
use App\Repositories\PostsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var PostsRepositoryInterface
     */
    private $postsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                PostsRepositoryInterface $postsRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->postsRepository = $postsRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        $posts = $this->postsRepository->getAllPosts();

        return view('home.posts.index')->with(
            [
                'posts' => $posts,
            ]
        );
    }

    public function show(Post $post)
    {
        $action = 'ui-show';
        $this->trafficRepository->getRemoteAddress($action);

        $lastTag = $this->postsRepository->getLastTag($post);

        return view('home.posts.show')->with(
            [
                'post' => $post,
                'lastTag' => $lastTag,
            ]
        );
    }

    public function tube()
    {
        $action = 'ui-tube';
        $this->trafficRepository->getRemoteAddress($action);

        $tubes = $this->postsRepository->getAllTubes();

        return view('home.youtube.index')->with(
            [
                'posts' => $tubes,
            ]
        );
    }

    public function tubeShow(Tube $tube)
    {
        $action = 'ui-tube-show';
        $this->trafficRepository->getRemoteAddress($action);

        $lastTag = $this->postsRepository->getLastTubeTag($tube);

        return view('home.youtube.show')->with(
            [
                'post' => $tube,
                'lastTag' => $lastTag,
            ]
        );
    }

}
