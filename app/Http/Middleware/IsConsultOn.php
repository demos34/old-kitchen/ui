<?php

namespace App\Http\Middleware;

use App\Models\Consult\ConsultActivity;
use Closure;
use Illuminate\Http\Request;

class IsConsultOn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $status = ConsultActivity::first();
        if($status->is_on == 0) {
            abort(403, 'В момента консултациите са изключени');
        }

        return $next($request);
    }
}
