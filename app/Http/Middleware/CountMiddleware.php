<?php

namespace App\Http\Middleware;

use App\Models\Analyst\Site;
use App\Models\Count\Count;
use App\Models\Count\Countable;
use App\Models\Count\CountType;
use App\Models\Recipes\DraftRecipe;
use App\Models\Recipes\Recipe;
use Closure;
use Illuminate\Http\Request;

class CountMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $type = $this->getType();
        $countable = $this->getCountable($type);

        $referer = $_SERVER['REQUEST_URI'] ?? null;
        $site = Site::where('url', $referer)->firstOrCreate(
            [
                'url' => $referer,
            ],
            [
                'string_id' => bin2hex(openssl_random_pseudo_bytes(20)),
            ]
        );

        Count::create(
            [
                'count_type_id' => $type,
                'site_id' => $site->id,
                'countable' => $countable,
                'count' => 1,
            ]
        );

        $countType = CountType::findOrFail($type);

        if($countType->id > 1 || $countType->id < 6)
        {
            if($countable !== 'first' && $countable !== '' && $countable !== 'first' && $countable !== 'NONE')
            {
                if($countType->countables()->where('countable', $countable)->first())
                {
                    $toUpdate = $countType->countables()->where('countable', $countable)->first();
                    $toUpdate->update(
                        [
                            'counts' => $toUpdate->counts + 1,
                        ]
                    );
                } else {
                    $countType->countables()->create(
                        [
                            'countable' => $countable,
                        ]
                    );
                }
            }
        }

        return $next($request);
    }

    protected function getType(): int
    {
        $path = $this->getPath();
        if($path !== null) {
            $type = explode('/', $path)[1];
        } else {
            $type = 'NONE';
        }

        if($type === 'types') {
            return 2;
        } elseif ($type === 'recipes') {
            return 3;
        } elseif ($type === 'posts') {
            return 4;
        } elseif ($type === 'tube') {
            return 5;
        } elseif ($type === 'contacts' || $type === 'consultations' || $type === 'books') {
            return 6;
        } else {
            return 1;
        }
    }

    protected function getCountable(int $type)
    {
        $path = $this->getPath();
        $countable = null;
        if($path !== null){
            $countable = explode('/', $path)[2] ?? null;
            $ids = explode('-', $countable);

        }

        if ($type === 2)
        {
            return $ids[0];
        }
        elseif ($type === 3)
        {
            $draft = DraftRecipe::find($ids[0]);
            return Recipe::where('draft_id', $draft->id)->first()->id;
        }
        elseif ($type === 4 || $type === 5)
        {
            if(end($ids) === ""){
                return 'NONE';
            }
            return end($ids);
        }
        else
        {
            return 'NONE';
        }
    }

    protected function getPath()
    {
        return $_SERVER['REQUEST_URI'] ?? null;
    }

}
