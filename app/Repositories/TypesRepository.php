<?php


namespace App\Repositories;


use App\Models\Letters\Letter;
use App\Models\Recipes\Recipe;
use App\Models\Type\Type;

class TypesRepository implements TypesRepositoryInterface
{
    public function getLetters()
    {
        return Letter::all();
    }

    public function getRecipesByFirstLetter(Type $type, Letter $letter)
    {
        return $type->recipes()->where('title', 'like', $letter->letter.'%')->simplePaginate(15);
    }
}
