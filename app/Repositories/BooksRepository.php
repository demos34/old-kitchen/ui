<?php


namespace App\Repositories;


use App\Models\Book\Book;
use App\Models\Book\GetOne;
use App\Models\Book\Message;
use Illuminate\Http\Request;

class BooksRepository implements BooksRepositoryInterface
{
    public function getAllBooks()
    {
        return Book::all();
    }

    public function sendUsMsgToGetBook(Request $request, Book $book)
    {
        return $this->addMSG($request, $book);
    }

    protected function addMSG(Request $request, Book $book)
    {
        $validated = self::validateRequest($request);
        if (self::storeGetOne($validated, $book)){
            $msg = self::msg('success', 'Вашето запитване е изпратено! Ние ще го разгледаме и ще ви отговорим възможно най-скоро!');
        } else {
            $msg = self::msg('alert', 'Нещо се обърка! Моля опитайте пак по-късно!');

        }
        self::sendMessageToAdministrator($validated, $book);

        return $msg;
    }

    protected static function validateRequest(Request $request)
    {
        return $request->validate(
            [
                'names' => 'required|string|min:5|max:100',
                'email' => 'required|email|min:5|max:100',
                'additional' => 'max:1000',
            ]
        );
    }

    protected static function storeGetOne($validated, Book $book)
    {
        return GetOne::create(
            [
                'from' => $validated['names'],
                'email' => $validated['email'],
                'additional' => $validated['additional'],
                'book_id' => $book->id,
            ]
        );
    }

    protected static function msg($session, $message)
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }

    protected static function sendMessageToAdministrator($validated, Book $book)
    {
        $message = 'Има нова поръчка от: ';
        $message .= $validated['names'];
        $message .= 'с имейл: ' . $validated['email'] . PHP_EOL;
        $message .= 'Допълнителна информация:' . PHP_EOL;
        $message .= 'За книга:' . PHP_EOL;
        $message .= $book->title;

        $newMessage = Message::create(
            [
                'from' => 'System - купуване на книга',
                'message' => $message,
                'type' => 1,
                'is_read' => false,
            ]
        );

        $newMessage->messageTypes()->attach(1);
    }

}
