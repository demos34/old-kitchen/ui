<?php

namespace App\Repositories;

use App\Models\Home\Post;
use App\Models\Home\Tube;

interface PostsRepositoryInterface
{
    public function getAllPosts();

    public function getAllTubes();

    public function getLastTag(Post $post);

    public function getLastTubeTag(Tube $tube);

    public function getAllKeywords();
}
