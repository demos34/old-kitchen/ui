<?php


namespace App\Repositories;


use App\Models\Recipes\Recipe;
use App\Models\Recipes\TimeToCook;
use App\Models\Type\Type;

class RecipesRepository implements RecipesRepositoryInterface
{
    public function getType(Recipe $recipe)
    {
        $typeId = implode(', ', $recipe->types()->get()->pluck('id')->toArray());
        return Type::where('id', $typeId)->firstOrFail();
    }

    public function getAllRecipesByType(Type $type)
    {

    }

    public function getAllRecipes()
    {
        return Recipe::orderByDesc('updated_at')->paginate(20);
    }

    public function getLastRecipes($howMany)
    {

    }

    public function getLastNRecipes($n)
    {
        return Recipe::orderByDesc('created_at')->limit($n)->get();
    }

    public function getTimeToCook($draftId)
    {
        return TimeToCook::where('draft_recipe_id', $draftId)->get();
    }

}
