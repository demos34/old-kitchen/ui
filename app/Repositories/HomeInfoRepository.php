<?php
namespace App\Repositories;

use App\Models\Book\Message;
use App\Models\Home\HomeInfo;
use App\Models\Home\IndexTag;
use App\Models\Partners\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeInfoRepository implements HomeInfoRepositoryInterface
{
    public function getHomeInfo()
    {
        return HomeInfo::find('1');
    }

    public function getTags()
    {
        return IndexTag::all();
    }

    public function getLastTag()
    {
        return DB::table('index_tags')->orderBy('id', 'DESC')->first();
    }

    public function contacts(Request $request)
    {
        return self::sendUsMsg($request);
    }

    public function getAllPartners()
    {
        return Partner::all();
    }

    protected static function sendUsMsg(Request $request)
    {
        $validated = self::validate($request);
        self::storeMsg($validated);
        return self::msg('success', 'Вашето съобщение е изпратено!');

    }

    protected static function validate(Request $request)
    {
        return $request->validate(
            [
                'email' => 'required|email|min:5|max:100',
                'msg' => 'required|string|min:5|max:1000',
            ]
        );
    }

    protected static function storeMsg($validated)
    {
        $message = 'Имате съобщение от:';
        $message .= $validated['email'] . PHP_EOL;
        $message .= 'Съобщение:' . PHP_EOL;
        $message .= $validated['msg'];

        $newMessage =  Message::create(
            [
                'from' => 'System - съобщение от контакти:',
                'message' => $message,
                'type' => 2,
                'is_read' => false,
            ]
        );

        $newMessage->messageTypes()->attach(2);
    }

    protected static function msg($session, $message)
    {
        return [
            'session' => $session,
            'message' => $message
        ];
    }

}
