<?php

namespace App\Repositories;

use App\Models\Consult\ConsultType;
use Illuminate\Http\Request;

interface ConsultRepositoryInterface
{
    public function getType($type);

    public function getRecipes();

    public function getServices(ConsultType $consultType);

    public function getCategories();

    public function getSpices();

    public function getAllComments(ConsultType $consultType);

    public function  store(Request $request, ConsultType $consultType);

    public function storeComment(Request $request, ConsultType $consultType);
}
