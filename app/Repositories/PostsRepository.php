<?php
namespace App\Repositories;


use App\Models\Home\Post;
use App\Models\Home\Tube;
use App\Models\Tag\Keyword;
use App\Models\Tag\Tag;
use Illuminate\Support\Facades\DB;

class PostsRepository implements PostsRepositoryInterface
{
    public function getAllPosts()
    {
        return Post::orderBy('created_at', 'desc')->paginate(9);
    }

    public function getAllTubes()
    {
        return Tube::orderBy('created_at', 'desc')->paginate(9);
    }

    public function getLastTag(Post $post)
    {
        $pivot = DB::table('keyword_post')->where('post_id', $post->id)->orderBy('created_at', 'desc')->limit(1)->first();

        return Keyword::findOrFail($pivot->keyword_id);
    }

    public function getLastTubeTag(Tube $tube)
    {
        $pivot = DB::table('keyword_tube')->where('tube_id', $tube->id)->orderBy('created_at', 'desc')->limit(1)->first();

        return Keyword::findOrFail($pivot->keyword_id);
    }

    public function getAllKeywords()
    {
        return Keyword::all();
    }
}
