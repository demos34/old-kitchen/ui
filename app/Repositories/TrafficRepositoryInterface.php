<?php
namespace App\Repositories;

interface TrafficRepositoryInterface
{
    public function getRemoteAddress($action);
}