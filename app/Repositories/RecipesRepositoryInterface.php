<?php

namespace App\Repositories;

use App\Models\Recipes\Recipe;
use App\Models\Type\Type;

interface RecipesRepositoryInterface
{
    public function getType(Recipe $recipe);

    public function getAllRecipesByType(Type $type);

    public function getAllRecipes();

    public function getLastRecipes($howMany);

    public function getLastNRecipes($n);

    public function getTimeToCook($draftId);
}
