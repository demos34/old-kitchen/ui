<?php

namespace App\Repositories;

use App\Models\Letters\Letter;
use App\Models\Type\Type;

interface TypesRepositoryInterface
{
    public function getLetters();

    public function getRecipesByFirstLetter(Type $type, Letter $letter);
}
