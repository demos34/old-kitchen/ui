<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 25.5.2020 г.
 * Time: 20:16
 */

namespace App\Repositories;

use Illuminate\Http\Request;

interface HomeInfoRepositoryInterface
{
    public function getHomeInfo();

    public function getTags();

    public function getLastTag();

    public function contacts(Request $request);

    public function getAllPartners();
}
