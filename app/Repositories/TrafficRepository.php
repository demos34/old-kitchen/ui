<?php
namespace App\Repositories;


use App\Models\Traffic\Traffic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TrafficRepository implements TrafficRepositoryInterface
{
    public function getRemoteAddress($action)
    {

        /*
         * actions:
         * index /class get
         * create /class/create get
         * store /class post
         * show /class/id get
         * edit /class/id/edit get
         * update /class/ id put
         * destroy /class/id delete
         */
        if(isset(Auth::user()->id)) {
            $userId = Auth::user()->id;
            $username = Auth::user()->username;
            $userRole = Auth::user()->roles->first()->role;
        } else {
            $userId = 'Guest';
            $username = 'Guest';
            $userRole = 'Guest';
        }

        $remoteAddress = $_SERVER['REMOTE_ADDR'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $httpCookie = 'not set';
//        $httpCookie = $_SERVER['HTTP_COOKIE'];
        $previousURL = 'not set url';
//        if(!empty(Session::previousUrl())){
//            $previousURL = Session::previousUrl();
//        } else {
//            $previousURL = 'not set url';
//        }

        $method = 'method';
        $traffic = $_SERVER['REQUEST_URI'];
        $args = explode("/", $traffic);
        $directory = $args['1'];
        if(count($args) < 3) {

            $directory = 'home';
            $class = 'HomeController';
        } else {
            $class = ucfirst($args['2']) . 'Controller';
        }
        $id = '0';

        if($action == 'index'){
            $method = 'get';
        } elseif ($action == 'create'){
            $method = 'get';
        } elseif ($action == 'store'){
            $method = 'post';
        } elseif ($action =='show'){
            $method = 'get';
        } elseif ($action == 'edit') {
            $method = 'get';
            $id = $args['3'];
        } elseif ($action == 'update'){
            $method = 'patch';
            $id = $args['3'];
        } elseif ($action == 'destroy') {
            $method = 'delete';
            $id = $args['3'];
        }

        return Traffic::create(
            [
                'username' => $username,
                'user_id' => $userId,
                'user_role' =>  $userRole,
                'remote_address' => $remoteAddress,
                'user_agent' => $userAgent,
                'http_cookie' =>  $httpCookie,
                'previous_url' => $previousURL,
                'method' => $method,
                'request_uri' => $traffic,
                'directory' => $directory,
                'class' => $class,
                'action' => $action,
                'given_id_in_action' => $id
            ]
        );


    }
}
