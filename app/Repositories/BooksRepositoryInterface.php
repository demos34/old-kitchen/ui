<?php

namespace App\Repositories;

use App\Models\Book\Book;
use Illuminate\Http\Request;

interface BooksRepositoryInterface
{
    public function getAllBooks();

    public function sendUsMsgToGetBook(Request $request, Book $book);
}
