<?php


namespace App\Repositories;


use App\Models\Consult\BusinessMessage;
use App\Models\Consult\Client;
use App\Models\Consult\ClientCategoryAntherDish;
use App\Models\Consult\ClientInformation;
use App\Models\Consult\ConsultCategory;
use App\Models\Consult\ConsultComment;
use App\Models\Consult\ConsultMessage;
use App\Models\Consult\ConsultService;
use App\Models\Consult\ConsultSubcategory;
use App\Models\Consult\ConsultType;
use App\Models\Consult\Spice;
use App\Models\Recipes\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ConsultRepository implements ConsultRepositoryInterface
{
    public function getType($type)
    {
        return ConsultType::where('slug', $type)->firstOrFail();
    }

    public function getRecipes()
    {
        return Recipe::inRandomOrder()->limit(5)->get();
    }

    public function getServices(ConsultType $consultType)
    {
        return ConsultService::where('consult_type_id', $consultType->id)->get();
    }

    public function getCategories()
    {
        return ConsultCategory::all();
    }

    public function getSpices()
    {
        return Spice::all();
    }

    public function getAllComments(ConsultType $consultType)
    {
        return ConsultComment::where('is_visible', true)
            ->where('consult_type_id', $consultType->id)
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
    }

    public function store(Request $request, ConsultType $consultType)
    {
        $validatedClient = self::validateClientPersonalInfo($request, $consultType);
        if (!$client = self::storeValidatedClient($validatedClient, $consultType)) return false;
        if($consultType->id == 1) {
            $validatedInfo = self::validateClientInformation($request);
            self::storeValidatedClientInformation($validatedInfo, $client);
            self::storeCategoriesAndSubcategoriesForClient($request, $client);
        } else {
            self::storeBusinessMessage($validatedClient, $client);
        }
        self::sendConsultMessage($client, $consultType);
        $client->consultStatuses()->attach(1);

        return true;
    }

    public function storeComment(Request $request, ConsultType $consultType): array
    {
        $validated = self::validateComment($request);
        return self::storeValidatedComment($validated, $consultType->id);

    }

    protected static function validateComment(Request $request): array
    {
        return $request->validate(
            [
                'sender' => 'required|min:3|max:50|string',
                'message' => 'required|min:3|max:2000',
            ]
        );
    }

    protected static function storeValidatedComment($validated, $consultId): array
    {
        $sessionId = self::getSessionId();
        if(ConsultComment::where('session_id', $sessionId)->first() !== NULL){
            $comment = ConsultComment::where('session_id', $sessionId)->latest()->first();
            $expiry = $comment->created_at->addMinutes(5);
            if (now() < $expiry) {
                return self::msg('warning', 'Трябва да изчакате 5 минути преди да пуснете отново отзив!');
            }
        }
        if(ConsultComment::create(
            [
                'message' => $validated['message'],
                'from' => $validated['sender'],
                'is_visible' => true,
                'session_id' => self::getSessionId(),
                'consult_type_id' => $consultId,

            ]
        )){
            $msg = self::msg('success', 'Благодарим за Вашия коментар!');
        } else {

            $msg = self::msg('alert', 'Опа! Получи се проблем с коментара и той не беше доставен! Моля опитайте пак!');
        }
        return $msg;
    }

    protected static function getSessionId(): string
    {
        return session()->getId();
    }

    protected static function validateClientPersonalInfo(Request $request, ConsultType $consultType): array
    {
        if ($consultType->id == 1) {
            return $request->validate(
                [
                    'first_name' => 'required|string|min:3|max:20',
                    'last_name' => 'required|string|min:3|max:20',
                    'country' => 'nullable|min:1|max:30|string',
                    'city' => 'nullable|min:1|max:30|string',
                    'address_one' => 'nullable|min:1|max:200|string',
                    'address_two' => 'nullable|min:1|max:200|string',
                    'email' => 'required|min:3|max:50|email',
                    'phone' => 'required|numeric|min:5',
                    'start_date' => 'required|date||after:today',
                    'company' => 'nullable|min:1|max:255|string',
                    'type' => 'nullable|min:1|max:255|string',
                ]
            );
        } else {
            return $request->validate(
                [
                    'first_name' => 'required|string|min:3|max:20',
                    'last_name' => 'required|string|min:3|max:20',
                    'country' => 'nullable|min:1|max:30|string',
                    'city' => 'nullable|min:1|max:30|string',
                    'address_one' => 'nullable|min:1|max:200|string',
                    'address_two' => 'nullable|min:1|max:200|string',
                    'email' => 'required|min:3|max:50|email',
                    'phone' => 'required|numeric|min:5',
                    'start_date' => 'required|date||after:today',
                    'company' => 'required|min:1|max:255|string',
                    'type' => 'nullable|min:1|max:255|string',
                    'info' => 'required|string|min:1|max:2000',
                ]
            );
        }
    }

    protected static function storeValidatedClient($validatedClient, ConsultType $consultType)
    {
        $session = session()->getId();

        if ($client = Client::where('session_id', $session)->first()) {
            $expiry = $client->created_at->addMinutes(30);
            if (now() < $expiry) return false;
            $client->delete();
        }
        return Client::create(
            [
                'first_name' => $validatedClient['first_name'],
                'last_name' => $validatedClient['last_name'],
                'country' => $validatedClient['country'],
                'city' => $validatedClient['city'],
                'address_one' => $validatedClient['address_one'],
                'address_two' => $validatedClient['address_two'],
                'email' => $validatedClient['email'],
                'phone' => $validatedClient['phone'],
                'start_date' => $validatedClient['start_date'],
                'session_id' => $session,
                'consult_type_id' => $consultType->id,
            ]
        );
    }

    protected static function validateClientInformation(Request $request): array
    {
        return $request->validate(
            [
                'spice' => 'required|exists:spices,id',
                'fav_foods' => 'nullable|string|min:3|max:200',
                'fav_rest' => 'nullable|string|min:3|max:200',
                'allergic' => 'required|string|min:1|max:200',
                'info' => 'required|string|min:1|max:2000',
            ]
        );
    }

    protected static function storeValidatedClientInformation($validated, Client $client)
    {
        return ClientInformation::create(
            [
                'client_id' => $client->id,
                'spice_id' => $validated['spice'],
                'fav_foods' => $validated['fav_foods'],
                'fav_rest' => $validated['fav_rest'],
                'allergic' => $validated['allergic'],
                'info' => $validated['info'],
            ]
        );
    }

    protected static function storeCategoriesAndSubcategoriesForClient(Request $request, Client $client): array
    {
        $consultCategories = ConsultCategory::all();
        $msg = [];
        foreach ($consultCategories as $category) {
            $cat = $category->name;
            if ($request->has($cat)) {
                $client->consultCategories()->attach($category->id);
                $isAnotherField = false;
                foreach ($request->$cat as $value) {
                    if ($value === $cat) $isAnotherField = true;
                }
                if ($isAnotherField) {
                    $descr = $category->name . '_descr';
                    self::storeInAnotherCategoryTable($client->id, $category->id, $request->$descr);
                    $count = count($request->$cat);
                    $existedElements = [];
                    for ($i = 0; $i < $count - 1; $i++) {
                        $elId = $request->$cat[$i];
                        if (ConsultSubcategory::find($elId)) array_push($existedElements, $elId);
                    }
                    $client->consultSubcategories()->attach($existedElements);
                } else {
                    $validatedSubsId = $request->validate(
                        [
                            $cat => 'required|exists:consult_subcategories,id',
                        ]
                    );
                    foreach ($validatedSubsId as $item) {
                        $client->consultSubcategories()->attach($item);
                    }
                }
            }
        }
        return $msg;
    }

    protected static function storeInAnotherCategoryTable($clientId, $categoryId, $description)
    {

        ClientCategoryAntherDish::create(
            [
                'client_id' => $clientId,
                'consult_category_id' => $categoryId,
                'description' => $description,
            ]
        );
    }

    protected static function sendConsultMessage(Client $client, ConsultType $consultType)
    {
        $message = $client->first_name . ' ' . $client->last_name . ' Поиска консултация за крайна дата ' . $client->start_date;
        $msg = ConsultMessage::create(
            [
                'client_id' => $client->id,
                'message' => $message,
                'is_read' => false,
                'is_business' => false,
            ]
        );
        if($consultType->id == 2) $msg->update(['is_business' => true,]);
    }

    protected static function storeBusinessMessage($validated, Client $client)
    {
        BusinessMessage::create(
            [
                'client_id' => $client->id,
                'from' => $client->first_name . ' ' . $client->last_name,
                'company' => $validated['company'],
                'email' => $client->email,
                'phone' => $client->phone,
                'session_id' => $client->session_id,
                'type' => $validated['type'],
                'message' => $validated['info'],
            ]
        );
    }

    protected static function msg($session, $message): array
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }
}
