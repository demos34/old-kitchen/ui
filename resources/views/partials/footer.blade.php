<footer class="footer-class pt-20 pt-10">
    <div class="d-flex justify-content-between mt-5 footer-items-wrapper">
        <div id="footer-image">
            <img src="/storage/logo/logo-new.png" class="footer-image">
        </div>
        <div class="policy">
            <div>
                <span>
                    Общи условия, политика за сигурност, политика за бисквитките
                </span>
            </div>
            <a href="{{route('security')}}">Политика за сигурност</a>
            <a href="{{route('terms')}}">Общи правила и условия</a>
            <a href="{{route('cookies')}}">Политика за бисквитките</a>
        </div>
        <div class="contacts-footer">
            <div>
                <span>
                    Може да ни намерите на:
                </span>
            </div>
            <div class="footer-icons">
                <div>
                    <a target="_blank" href="https://www.facebook.com/staratakuhnia">
                        <img class="contacts-images-footer" src="/storage/home/fb.png" alt="fb">
                    </a>
                </div>
                <div>
                    <a target="_blank" href="https://www.instagram.com/staratakuhnia/?fbclid=IwAR0pMwOIZIat-NFy9tFlubAi6WpR8HTNIemowD-yPS7wVuV5X9vKbYC0WSA">
                        <img class="contacts-images-footer" src="/storage/home/in.png" alt="insta">
                    </a>
                </div>
                <div>
                    <a target="_blank" href="https://www.youtube.com/channel/UCGQNduHc9DOfOY-vQtqWx7A?view_as=subscriber&fbclid=IwAR3aHIjCygG6qp3boOaSdp5j3h7pfNSaMwTBYSp8-mUsF9GHHZbFjqXwirQ">
                        <img class="contacts-images-footer" src="/storage/home/tube.png" alt="tube">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
