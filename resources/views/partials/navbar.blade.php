<nav class="navbar navbar-expand-md navbar-light w-100">
    <div class="navbar-custom">
        <a class="navbar-brand" href="{{ url('/') }}">
            <div class="d-flex justify-content-between">
                <div class="mx-1">
                    <img src="{{ $homeInfo->logo_image_url }}" style="max-height: 60px;" alt="123">
                </div>
{{--                <div class="my-auto mx-1">--}}
{{--                    {{ config('app.name', 'Старата кухня') }}--}}
{{--                </div>--}}
            </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
{{--            <span class=".navbar-toggle .icon-bar"></span>--}}
            <span class="navbar-toggler-icon nav-button"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown custom-nav-li">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle custom-a-drop" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Рецепти
                        </a>
                        <div class="dropdown-menu dropdown-menu-left dropdown-div-custom" aria-labelledby="navbarDropdown">
                            @if($types->count()>0)
                                @foreach($types as $type)
                                <a class="dropdown-item dropdown-item-custom" href="{{route('types-show', $type->slug)}}">
                                    {{ $type->type }}
                                </a>
                                @endforeach
                            @endif
                            <a class="dropdown-item dropdown-item-custom" href="{{route('recipes')}}">
                                Виж всички
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown custom-nav-li">
                        <a id="navbarDropdown-project" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Проекти
                        </a>
                        <div class="dropdown-menu dropdown-div-custom" aria-labelledby="navbarDropdown-project">
                            <a class="dropdown-item dropdown-item-custom" href="{{route('books-index')}}">
                                Книги
                            </a>
                            <a class="dropdown-item dropdown-item-custom" href="">
                                Нови
                            </a>
                        </div>
                    </li>
                    <li class="nav-item custom-nav-li">
                        <a class="nav-link" href="{{route('partners-index')}}" role="button">
                            Партньори
                        </a>
                    </li>
                    @if($on->is_on == true)
                        <li class="nav-item custom-nav-li">
                            <a class="nav-link" href="{{route('consult-index')}}" role="button">
                                Консултации
                            </a>
                        </li>
                    @endif
                    <li class="nav-item dropdown custom-nav-li">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle custom-a-drop" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Знания
                        </a>
                        <div class="dropdown-menu dropdown-menu-left dropdown-div-custom" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item dropdown-item-custom" href="{{route('posts-index')}}">
                                Блог
                            </a>
                            <a class="dropdown-item dropdown-item-custom" href="{{route('tube-index')}}">
                                Youtube канал
                            </a>
                        </div>
                    </li>
                    <li class="nav-item custom-nav-li">
                        <a class="nav-link" href="{{route('contacts')}}" role="button">
                            Контакти
                        </a>
                    </li>
                </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-5">
                <!-- Authentication Links -->
                <li>
                    <form action="{{route('search')}}" method="post">
                        @csrf
                        <label for="search"></label>
                        <input name="search" id="search" type="text" class="search-input mr-0">
                        <button class="search-button">
                            <img src="/storage/logo/search.svg" alt="search">
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>
