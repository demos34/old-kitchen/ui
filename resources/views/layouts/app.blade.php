
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
@yield("meta")
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

@yield("title")

<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('partials.custom-style')
    @yield("style")
</head>
<body>
@include('partials.navbar')
@include('cookieConsent::index')

<div id="app" >
    <main>
        @include('partials.alerts')
        @yield('content')
    </main>
</div>

@include('partials.footer')

@yield("scripts")
</body>
</html>
