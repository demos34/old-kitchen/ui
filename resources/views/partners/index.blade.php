@extends('layouts.app')

@section('meta')
    <meta name="description" content="Хранилище">
    <meta name="keywords"
          content="партньори">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня| Партньори
    </title>
@endsection

@section('content')
    <div class="top-image-partners">
        <img id="top-image-partners" alt="top-image" src="/storage/partners/partners.png">
    </div>
    <div class="w-100" id="books">
        <div class="text-center mt-5">
            <p class="book-title">
                ПАРТНЬОРИ
            </p>
        </div>
        <div class="books-wrapper text-center">
            @foreach($partners as $partner)
                <div class="text-center my-0">
                    <p class="book-title">
                        {{$partner->title}}
                    </p>
                </div>
                <div class="">
                    <img class="partner-image" src="{{$partner->image}}" alt="book-image-{{$partner->slug}}">
                </div>
                <div class="text-center book-description">
                    {{$partner->description}}
                </div>
                    <div class="mx-auto">
                        <a href="{{route('partners-show', $partner->slug)}}">
                            <button class="books-button">
                                Още...
                            </button>
                        </a>
                    </div>
                </div>
                <div class="book-bottom">
                </div>
            @endforeach
        </div>
    </div>
@endsection
