@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$partner->description}}">
    <meta name="keywords"
          content="Партньори | {{$partner->title}}">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня| {{$partner->title}}
    </title>
@endsection

@section('content')
    <div class="w-100" id="book">
        <div class="book-show-wrapper-show mx-auto">
            <div class="book-image-show mx-auto">
                <img src="{{$partner->image}}" alt="show-image">
            </div>
            <div class="book-show-title">
                <span>
                    {{$partner->title}}
                </span>
            </div>
            <div class="text-center w-100">
                <div class="book-show-description text-center">
                    <span>
                        {{$partner->description}}
                    </span>
                </div>
            </div>
            <div class="text-center book-get">
                <br>
                <a href="{{$partner->url}}">
                    <button>
                        Към сайта на {{$partner->title}}:
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
