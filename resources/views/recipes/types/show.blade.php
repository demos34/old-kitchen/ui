@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$type->description}}">
    <meta name="keywords"
          content="@foreach($type->keywords as $tag) {{$tag->name}}@endforeach">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | {{$type->type}}
    </title>
@endsection

@section('content')
    <div class="text-center my-5">
        <div class="mx-auto types-images-div text-center">
            <img src="{{ implode(', ', $type->images()->get()->pluck('url')->toArray())}}" alt="type-image"
                 class="type-image">
        </div>
        <div class="container">
            <div class="m-5">
                @foreach($letters as $letter)
                    @foreach($type->recipes as $recipe)
                        @if($letter->letter == mb_substr($recipe->title, 0, 1))
                            @if($count == 0)
                                <div class="mb-2 w-100">
                                    <span class="types-letter">
                                        <a href="{{route('types-letter', [$type->slug, $letter->letter])}}">
                                            {{$letter->letter}}
                                        </a>
                                    </span>
                                    <div class="type-letter-ruler">
                                    </div>
                                    <br>
                                </div>
                            <div class="row mt-5 mx-auto">
                                    @endif
                            @if($count == 3)
                                <?php break; ?>
                            @endif
                                    <div class="col-md-3 offset-1 type-recipes-wrapper mx-auto">
                                        <div>
                                            <a href="{{route('recipes-show', $recipe->slug)}}">
                                                <img class="index-last-recipe-image"
                                                     src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}"
                                                     alt="index-last-recipe">
                                            </a>
                                        </div>
                                        <div class="index-title type-index-title">
                                            <h1 class="font-weight-bold text-center">{{$recipe->title}}</h1>
                                            <span class="type-index-title-mobile-span">
                                                {{$recipe->title}}
                                            </span>
                                        </div>
                                    </div>
                            @if($count == 3)
                                <?php break; ?>
                            </div>
                            @endif
                            <?php $count++ ?>
                        @endif
                    @endforeach
                    <?php $count = 0 ?>
                @endforeach
            </div>
        </div>
@endsection
