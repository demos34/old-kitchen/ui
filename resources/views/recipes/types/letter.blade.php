@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$type->description}}">
    <meta name="keywords"
          content="@foreach($type->keywords as $tag) {{$tag->name}}@endforeach">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | {{$type->type}} с {{$letter->letter}}
    </title>
@endsection

@section('content')
    <div class="container text-center">
        <div class="mx-auto types-images-div">
            <img src="{{ implode(', ', $type->images()->get()->pluck('url')->toArray())}}" alt="type-image"
                 class="type-image">
            <div class="type-title">
                <div class="my-auto">
{{--                    {{$type->type}}--}}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="m-5">
                <div class="mb-2 w-100">
                    <span class="types-letter">
                        <a href="{{route('types-letter', [$type->slug, $letter->letter])}}">
                            {{$letter->letter}}
                        </a>
                    </span>
                    <div class="type-letter-ruler">
                    </div>
                    <br>
                </div>
                <div class="row mt-5 mx-auto">
                    @foreach($recipes as $recipe)
                        <div class="col-md-3 offset-1 type-recipes-wrapper mx-auto">
                            <div>
                                <a href="{{route('recipes-show', $recipe->slug)}}">
                                    <img class="index-last-recipe-image"
                                         src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}"
                                         alt="index-last-recipe">
                                </a>
                            </div>
                            <div class="index-title type-index-title">
                                <h1 class="font-weight-bold text-center">{{$recipe->title}}</h1>
                                <span class="type-index-title-mobile-span">
                                {{$recipe->title}}
                            </span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center">
                    {!! $recipes->links() !!}
                </div>
            </div>
        </div>
@endsection
