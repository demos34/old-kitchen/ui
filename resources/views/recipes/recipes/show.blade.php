@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$recipe->description}}">
    <meta name="keywords"
          content="@foreach($recipe->keywords as $tag) {{$tag->name}}@endforeach">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | {{$recipe->title}}
    </title>
@endsection

@section('content')
    <div class="container text-center my-5">
        <div class="mx-auto types-images-div">
            <img src="{{ implode(', ', $type->images()->get()->pluck('url')->toArray())}}" alt="type-image"
                 class="type-image">
        </div>
        <div class="recipes-title">
            <span>
                {{$recipe->title}}
            </span>
        </div>
        <div class="d-flex justify-content-between text-center mt-5">
            <div class="w-50">
                <div class="recipes-products">
                    <div class="recipes-products-span">
                <span>
                    Продукти:
                </span>
                    </div>
                    <div class="recipes-products-p">
                        @foreach($recipe->products as $product)
                            <p class="mt-1">
                                *{{$product->product}}
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="w-50">
                <div class="recipes-products">
                    <div class="recipes-products-span">
                <span>
                    Необходимо време:
                </span>
                    </div>
                    <div class="recipes-products-p">
                        @if((int)implode('',$time->pluck('days')->toArray()) > 0)
                            @if((int)implode('',$time->pluck('days')->toArray()) < 2)
                                {{(int)implode('',$time->pluck('days')->toArray())}} ден
                            @else
                                {{(int)implode('',$time->pluck('days')->toArray())}} дни
                            @endif
                        @endif
                        @if((int)implode('',$time->pluck('hours')->toArray()) > 0)
                            @if((int)implode('',$time->pluck('hours')->toArray()) < 2)
                                {{(int)implode('',$time->pluck('hours')->toArray())}} час
                            @else
                                {{(int)implode('',$time->pluck('hours')->toArray())}} часа
                            @endif
                        @endif
                        @if((int)implode('',$time->pluck('minute')->toArray()) > 0)
                            @if((int)implode('',$time->pluck('minute')->toArray()) < 2)
                                {{(int)implode('',$time->pluck('minute')->toArray())}} минута
                            @else
                                {{(int)implode('',$time->pluck('minute')->toArray())}} минути
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="recipes-how-to">
            <section class="text-center">
                <h2>
                    Начин на приготвяне
                </h2>
            </section>
            <section>
                @foreach(explode(PHP_EOL, $recipe->how_to) as $para)
                    <p>
                        {{$para}}
                    </p>
                @endforeach
            </section>
            <section>
                <img class="recipes-image-show"
                     src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}"
                     alt="index-last-recipe">
            </section>
            <section>

            </section>
        </div>
        </div>
    </div>
@endsection
