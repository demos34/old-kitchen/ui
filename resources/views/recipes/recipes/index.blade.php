@extends('layouts.app')

@section('meta')
    <meta name="description" content="Тук ще намерите всички рецепти от нашата Стара Кухня!">
    <meta name="keywords"
          content="всички рецепти на едно място">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Всички рецепти
    </title>
@endsection

@section('content')
    <div class="container text-center">
        <div class="row mt-5">
            @foreach($recipes as $recipe)
                <div class="col-md-3 offset-1">
                    <div>
                        <a href="{{route('recipes-show', $recipe->slug)}}">
                            <img class="index-last-recipe-image"
                                 src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}"
                                 alt="index-last-recipe">
                        </a>
                    </div>
                    <div class="index-title">
                        <h1 class="font-weight-bold text-center">{{$recipe->title}}</h1>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
