<div class="js-cookie-consent cookie-consent cookie-custom-css">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>
    <br>
    <button class="js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>
