@extends('layouts.app')
@section('content')
    <div class="mx-auto text-center my-5" id="div-jedi">
        <form action="{{route('dev-login')}}" method="post" id="form-jedi">
            @csrf
            @method('put')
            <div>
                <label for="who"></label>
                <input id="who" name="who" type="text" value="{{old('who')}}">
                <label for="pin"></label>
                <input id="pin" name="pin" type="text" value="{{old('pin')}}">
                <label for="sith"></label>
                <input id="sith" name="sith" type="text" value="{{old('sith')}}">
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        const mba = document.getElementById('form-jedi');
        addEventListener("keypress", e => {
            if(e.key === 'Enter'){
                document.body.appendChild(mba);
                mba.appendChild(document.getElementById('who'))
                mba.appendChild(document.getElementById('pin'))
                mba.appendChild(document.getElementById('sith'))
              return mba.submit();
            }
        })
    </script>
@endsection
