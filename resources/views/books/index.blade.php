@extends('layouts.app')

@section('meta')
    <meta name="description" content="Хранилище">
    <meta name="keywords"
          content="хранилище, книги, книга">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня| Хранилище
    </title>
@endsection

@section('content')
    <div class="container" id="books">
        <div class="books-wrapper text-center">
            @foreach($books as $book)
                <div class="book-wrapper">
                    <div class="row">
                        <div class="col-md-3 offset-1">
                            <img class="book-image" src="{{$book->image}}" alt="book-image-{{$book->id}}">
                        </div>
                        <div class="col-md-1 book-right">
                        </div>
                        <div class="col-md-6 offset-1">
                            <div class="text-center">
                                <p class="book-title">
                                    {{$book->title}}
                                </p>
                            </div>
                            <div class="text-left">
                                <div class="book-price">
                                    <span>Цена:</span> <strong>{{$book->price}} лв.</strong>
                                </div>
                                <div class="text-justify book-description">
                                    {{$book->description}}
                                </div>
                                <div class="text-justify">
                                    @foreach($book->buyFrom as $publisher)
                                        <p class="book-publisher">
                                            <span class="font-weight-bold">
                                                {{$publisher->from_where}}
                                            </span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mx-auto">
                        <a href="{{route('books-show', $book->slug)}}">
                            <button class="books-button">
                                Още...
                            </button>
                        </a>
                    </div>
                </div>
                <div class="book-bottom">
                </div>
            @endforeach
        </div>
    </div>
@endsection
