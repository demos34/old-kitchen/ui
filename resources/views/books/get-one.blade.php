@extends('layouts.app')

@section('meta')
    <meta name="description" content="Книги от старата кухня">
    <meta name="keywords"
          content="вземи, поръчай книгата">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня|Купи {{$book->title}}
    </title>
@endsection

@section('content')
    <div class="container" id="book">
        <div class="book-show-wrapper mx-auto w-75 text-center">
            <div class="text-center my-3">
                <strong class="send-us-msg-get-one">
                    Попълнете формата и ние ще се свържем с Вас!
                </strong>
            </div>
            <div class="text-center mx-auto d-flex justify-content-center">
                <form action="{{route('books-get-one-post', $book->slug)}}" method="POST" class="text-center mx-auto">
                    @csrf
                    @method('PUT')
                    <div class="w-100 text-left my-2">
                        <label for="names" class="get-one-label">
                            Три имена:
                        </label>
                        <input id="names" type="text" class="get-one-inputs form-control @error('names') is-invalid @enderror" name="names" value="{{ old('names') }}" required autocomplete="names">
                        @error('names')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="w-100 text-left my-2">
                        <label for="email" class="get-one-label">
                            Имейл:
                        </label>
                        <input id="email" type="email" class="get-one-inputs form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="emails">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="w-100 text-left my-2">
                        <label for="additional" class="get-one-label">
                            Допълнителна информация:
                        </label>
                        <textarea id="additional"
                                  name="additional"
                                  type="text"
                                  class="get-one-inputs form-control">{{ old('additional') }}</textarea>
                        @error('additional')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="text-center w-100">
                        <button class="books-get-one-button">
                            Изпрати
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
