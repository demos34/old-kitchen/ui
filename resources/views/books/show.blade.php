@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$book->description}}">
    <meta name="keywords"
          content="хранилище, книгата Хранилище">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня| {{$book->title}}
    </title>
@endsection

@section('content')
    <div id="book">
        <div class="book-show-wrapper mx-auto">
            <div class="book-image-show">
                <img src="{{$book->image}}" alt="show-image">
            </div>
            <div class="book-show-title">
                <span>
                    {{$book->title}}
                </span>
            </div>
            <div class="text-center w-100">
                <div class="book-show-description text-justify">
                    <span>
                        {{$book->description}}
                    </span>
                </div>
            </div>
            <div class="text-center book-get">
                <span>
                    Ако искате да ни подкрепите:
                </span>
                <br>
                <a href="{{route('books-get-one', $book->slug)}}">
                    <button>
                        Вземи книгата от тук
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
