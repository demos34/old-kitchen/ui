@extends('layouts.app')

@section('meta')
    <meta name="description" content="Това е youtube каналът към Рецепти от старата кухня!">
    <meta name="keywords"
          content="всички рецепти на едно място">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Youtube
    </title>
@endsection

@section('style')
    <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container w-100">
        <div class="blog-header">
            YouTube каналът
        </div>
        <div class="row my-5 blog-wrapper">
            @foreach($posts as $post)
                <div class="col-md-4 offset-1 mx-auto text-center">
                        <iframe class="w-75" src="{{$post->tube_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <br>
                    <a href="{{route('tube-show', $post->slug)}}">
                        <strong class="">
                            {{$post->title}}
                        </strong>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="text-center m-auto">
            {{$posts->links()}}
        </div>
    </div>
@endsection
