@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$post->description}}">
    <meta name="keywords"
          content="@foreach($post->keywords as $tag) @if($lastTag->id == $tag->id){{$tag->name}}@else{{$tag->name}}, @endif @endforeach">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Блог
    </title>
@endsection

@section('style')
    <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container w-100">
        <div class="blog-header">
            {{$post->title}}
        </div>
        <div class="blog-subtitle">
            {{$post->subtitle}}
        </div>
        <div class="row blog-author">
            <div class="col-lg-5 offset-1">
                Добавено от: {{$post->user->username}}
            </div>
            <div class="col-lg-5 offset-1">
                Добавено на: {{$post->created_at}}
            </div>
        </div>
        <div class="blog-image">
            <img class="w-100 mr-1" src="{{$post->image}}" alt="{{$post->image}}">
        </div>
        <div class="blog-body text-justify">
            @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                <p>
                    {{$paragraph}}
                </p>
            @endforeach
        </div>
    </div>
@endsection
