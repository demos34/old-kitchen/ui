@extends('layouts.app')

@section('meta')
    <meta name="description" content="Това е блогът към Рецепти от старата кухня!">
    <meta name="keywords"
          content="всички рецепти на едно място, блог, знания от старата кухня">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Блог
    </title>
@endsection

@section('style')
    <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container w-100">
        <div class="blog-header">
            Кът за знание
        </div>
        <div class="row my-5 blog-wrapper">
            @foreach($posts as $post)
                <div class="col-md-3 offset-1 mx-auto text-center">
                    <a href="{{route('posts-show', $post->slug)}}">
                        <img class="w-100" src="{{$post->image}}" alt="{{$post->image}}">
                        <br>
                        <strong class="">
                            {{$post->title}}
                        </strong>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="text-center m-auto">
            {{$posts->links()}}
        </div>
    </div>
@endsection
