@extends('layouts.app')

@section('meta')
    <meta name="description" content="Старата кухня - консултации за Вашето меню!">
    <meta name="keywords"
          content="консултации">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Консултации за вашето меню:
    </title>
@endsection

@section('content')
    <div class="consult-show-wrapper w-100">
        <div class="consult-form-main-wrapper-{{$type}} mx-auto" id="consult-form">
            <div class="form-wrapper-query" id="consult-query-button-{{$type}}">
                <div class="w-100 d-flex justify-content-end">
                    <div class="consult-form-close-button" id="consult-form-close-button">
                        &times;
                    </div>
                </div>
                <div class="desktop-query w-100">
                    <form action="{{route('consult-form-show-post', $type)}}" method="post">
                        @csrf
                        <div class="consult-content-title">
                            <strong>
                                Нов клиент:
                            </strong>
                        </div>
                        <div class="consult-content-name">
                            <strong>Имена*</strong>
                            <br>
                            <label for="first_name">Собствено</label>
                            <input type="text" id="first_name" class="@error('first_name') is-invalid @enderror"
                                   name="first_name" required autocomplete="first_name">
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <label for="last_name">Фамилно</label>
                            <input type="text" id="last_name" class="@error('last_name') is-invalid @enderror"
                                   name="last_name" required autocomplete="last_name">
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        @if($type == 'business')
                            <div class="consult-content-address">
                                <strong>Фирма/Компания/Предприятие и Тип събитие</strong>
                                <br>
                                <label for="company">Име*</label>
                                <input type="text" id="company" class="@error('company') is-invalid @enderror"
                                       name="company" required autocomplete="company">
                                @error('company')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="type">Тип събитие:</label>
                                <input type="text" id="type" class="@error('type') is-invalid @enderror" name="type"
                                       autocomplete="type">
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        @endif
                        <div class="consult-content-address">
                            <strong>Адрес</strong>
                            <br>
                            <label for="country">Държава</label>
                            <input type="text" id="country" class="@error('country') is-invalid @enderror"
                                   name="country" autocomplete="country">
                            @error('country')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <label for="city">Град</label>
                            <input type="text" id="city" class="@error('city') is-invalid @enderror" name="city"
                                   autocomplete="city">
                            @error('city')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <br>
                            <label for="address_one">Адрес</label>
                            <input type="text" id="address_one" class="@error('address_one') is-invalid @enderror"
                                   name="address_one" autocomplete="address_one">
                            @error('address_one')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <label for="address_two">Адрес 2</label>
                            <input type="text" id="address_two" class="@error('address_two') is-invalid @enderror"
                                   name="address_two" autocomplete="address_two">
                        </div>
                        <div class="consult-content-address">
                            <strong>Имейл (Електронна поща)*</strong>
                            <br>
                            <label for="email">Имейл</label>
                            <input type="email" id="email" class="@error('email') is-invalid @enderror" name="email"
                                   required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="consult-content-address">
                            <strong>Телефон*</strong> <small>Не използвайте интервали и/или тирета, напишете директно
                                номера - например 0888123456</small>
                            <br>
                            <label for="phone">Телефон:</label>
                            <input type="text" id="phone" class="@error('phone') is-invalid @enderror" name="phone"
                                   required autocomplete="phone">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        @if($type == 'beginner')
                            @foreach($categories as $category)
                                <div class="consult-content-category">
                                    <strong>{{$category->name}}*</strong>
                                    <br>
                                    @foreach($category->consultSubcategories as $subcategory)
                                        <div class="pl-5">
                                            <input type="checkbox" id="{{$category->name . '_' . $subcategory->id}}"
                                                   class="" name="{{$category->name}}[]" value="{{$subcategory->id}}">
                                            <label
                                                for="{{$category->name . '_' . $subcategory->id}}">{{$subcategory->name}}</label>
                                        </div>
                                    @endforeach
                                    <div class="pl-5">
                                        <input type="checkbox" id="{{$category->name . '_other'}}" class=""
                                               name="{{$category->name}}[]" value="{{$category->name}}">
                                        <label for="{{$category->name . '_other'}}">Друго?</label>
                                    </div>
                                    <div class="pl-5">
                                        <label for="{{$category->name . '_descr'}}">Ако сте избрали "Друго", моля
                                            опишете:</label>
                                        <br>
                                        <input value="{{old($category->name . '_descr')}}" type="text"
                                               id="{{$category->name . '_descr'}}"
                                               class="@error('$category->name' . '_descr') is-invalid @enderror consult-other-input"
                                               name="{{$category->name . '_descr'}}"
                                               autocomplete="{{$category->name . '_descr'}}">
                                        @error('$category->name' . '_descr')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            @endforeach
                            <div class="consult-content-address">
                                <strong>Люто?*</strong>
                                <br>
                                <label for="spice"></label>
                                <select id="spice" name="spice" class="">
                                    @foreach($spices as $spice)
                                        <option value="{{$spice->id}}">{{$spice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="consult-content-address">
                                <strong>Любими храни</strong>
                                <br>
                                <label class="consult-other-label" for="fav_foods">Моля, избройте любимите си
                                    храни:</label>
                                <br>
                                <input value="{{old('fav_foods')}}" type="text" id="fav_foods"
                                       class="@error('fav_foods') is-invalid @enderror consult-other-input"
                                       name="fav_foods" autocomplete="fav_foods">
                                @error('fav_foods')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="consult-content-address">
                                <strong>Любими ресторанти</strong>
                                <br>
                                <label class="consult-other-label" for="fav_rest">Моля, избройте любимите си
                                    ресторанти:</label>
                                <br>
                                <input value="{{old('fav_rest')}}" type="text" id="fav_rest"
                                       class="@error('fav_rest') is-invalid @enderror consult-other-input"
                                       name="fav_rest" autocomplete="fav_rest">
                                @error('fav_rest')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="consult-content-address">
                                <strong>Алергии към храни*</strong>
                                <br>
                                <label class="consult-other-label" for="allergic">Моля, избройте, ако имате, храни, към
                                    които имате алергии, a ако нямате, то просто напишете "Не":</label>
                                <br>
                                <input value="{{old('allergic')}}" type="text" id="allergic"
                                       class="@error('allergic') is-invalid @enderror consult-other-input"
                                       name="allergic" autocomplete="allergic">
                                @error('allergic')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        @endif
                        <div class="consult-content-address">
                            <strong>За дата*</strong>
                            <br>
                            <label class="consult-other-label" for="start_date">Моля, посочете дата, за която искате да
                                са готови рецептите:</label>
                            <br>
                            <input value="{{old('start_date')}}" type="datetime-local" id="start_date"
                                   class="@error('start_date') is-invalid @enderror consult-other-input"
                                   name="start_date" autocomplete="start_date">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="consult-content-address">
                            <strong>Допълнителна информация*</strong>
                            <br>
                            <label class="consult-other-label" for="info">Моля, ако има нещо, което сме пропуснали във
                                формуляра си, опишете го тук:</label>
                            <br>
                            <textarea id="allergic" class="@error('info') is-invalid @enderror consult-other-textarea"
                                      name="info" autocomplete="info">{{old('info')}}</textarea>
                            @error('info')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mx-auto text-center">
                            <button class="consult-form-submit-button-{{$type}}">
                                Изпрати заявката!
                            </button>
                        </div>
                    </form>
                </div>
                <div class="mobile-query">
                    <form action="{{route('consult-form-show-post', $type)}}" method="post">
                        @csrf
                        <div class="consult-content-title">
                            <strong>
                                Нов клиент:
                            </strong>
                        </div>
                        <div class="consult-content-name">
                            <strong>Имена*</strong>
                            <br>
                            <div class="mobile-content-name">
                                <div>
                                    <label for="first_name">Собствено</label>
                                    <input type="text" id="first_name" class="@error('first_name') is-invalid @enderror"
                                           name="first_name" required autocomplete="first_name">
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div>
                                    <label for="last_name">Фамилно</label>
                                    <input type="text" id="last_name" class="@error('last_name') is-invalid @enderror"
                                           name="last_name" required autocomplete="last_name">
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @if($type == 'business')
                            <div class="consult-content-name">
                                <strong>Фирма/Компания/Предприятие и Тип събитие</strong>
                                <br>
                                <div class="mobile-consult-content-address">
                                    <div>
                                        <label for="company">Име*</label>
                                        <input type="text" id="company" class="@error('company') is-invalid @enderror"
                                               name="company" required autocomplete="company">
                                        @error('company')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div>
                                        <label for="type">Тип събитие:</label>
                                        <input type="text" id="type" class="@error('type') is-invalid @enderror"
                                               name="type"
                                               autocomplete="type">
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="consult-content-address">
                            <strong>Адрес</strong>
                            <br>
                            <div class="mobile-consult-content-address">
                                <div>
                                    <label for="country">Държава</label>
                                    <input type="text" id="country" class="@error('country') is-invalid @enderror"
                                           name="country" autocomplete="country">
                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div>
                                    <label for="city">Град</label>
                                    <input type="text" id="city" class="@error('city') is-invalid @enderror" name="city"
                                           autocomplete="city">
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div>
                                    <label for="address_one">Адрес</label>
                                    <input type="text" id="address_one"
                                           class="@error('address_one') is-invalid @enderror" name="address_one"
                                           autocomplete="address_one">
                                    @error('address_one')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div>
                                    <label for="address_two">Адрес 2</label>
                                    <input type="text" id="address_two"
                                           class="@error('address_two') is-invalid @enderror" name="address_two"
                                           autocomplete="address_two">
                                    @error('address_two')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="consult-content-address">
                            <strong>Имейл (Електронна поща)*</strong>
                            <div class="mobile-consult-content-address">
                                <label for="email">Имейл</label>
                                <input type="email" id="email" class="@error('email') is-invalid @enderror" name="email"
                                       required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="consult-content-address">
                            <strong>Телефон*</strong> <small>Не използвайте интервали и/или тирета, напишете директно
                                номера - например 0888123456</small>
                            <div class="mobile-consult-content-address">
                                <div>
                                    <label for="phone">Телефон:</label>
                                    <input type="text" id="phone" class="@error('phone') is-invalid @enderror"
                                           name="phone" required autocomplete="phone">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @if($type == 'beginner')
                            @foreach($categories as $category)
                                <div class="consult-content-category">
                                    <strong>{{$category->name}}*</strong>
                                    <br>
                                    @foreach($category->consultSubcategories as $subcategory)
                                        <div class="pl-5">
                                            <input type="checkbox" id="{{$category->name . '_' . $subcategory->id}}"
                                                   class="" name="{{$category->name}}[]" value="{{$subcategory->id}}">
                                            <label
                                                for="{{$category->name . '_' . $subcategory->id}}">{{$subcategory->name}}</label>
                                        </div>
                                    @endforeach
                                    <div class="pl-5">
                                        <input type="checkbox" id="{{$category->name . '_other'}}" class=""
                                               name="{{$category->name}}[]" value="{{$category->name}}">
                                        <label for="{{$category->name . '_other'}}">Друго?</label>
                                    </div>
                                    <div class="pl-5">
                                        <label for="{{$category->name . '_descr'}}">Ако сте избрали "Друго", моля
                                            опишете:</label>
                                        <br>
                                        <input value="{{old($category->name . '_descr')}}" type="text"
                                               id="{{$category->name . '_descr'}}"
                                               class="@error('$category->name' . '_descr') is-invalid @enderror consult-other-input"
                                               name="{{$category->name . '_descr'}}"
                                               autocomplete="{{$category->name . '_descr'}}">
                                        @error('$category->name' . '_descr')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            @endforeach
                            <div class="consult-content-address">
                                <strong>Люто?*</strong>
                                <br>
                                <label for="spice"></label>
                                <select id="spice" name="spice" class="">
                                    @foreach($spices as $spice)
                                        <option value="{{$spice->id}}">{{$spice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="consult-content-address">
                                <strong>Любими храни</strong>
                                <br>
                                <label class="consult-other-label" for="fav_foods">Моля, избройте любимите си
                                    храни:</label>
                                <br>
                                <input value="{{old('fav_foods')}}" type="text" id="fav_foods"
                                       class="@error('fav_foods') is-invalid @enderror consult-other-input"
                                       name="fav_foods" autocomplete="fav_foods">
                                @error('fav_foods')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="consult-content-address">
                                <strong>Любими ресторанти</strong>
                                <br>
                                <label class="consult-other-label" for="fav_rest">Моля, избройте любимите си
                                    ресторанти:</label>
                                <br>
                                <input value="{{old('fav_rest')}}" type="text" id="fav_rest"
                                       class="@error('fav_rest') is-invalid @enderror consult-other-input"
                                       name="fav_rest" autocomplete="fav_rest">
                                @error('fav_rest')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="consult-content-address">
                                <strong>Алергии към храни*</strong>
                                <br>
                                <label class="consult-other-label" for="allergic">Моля, избройте, ако имате, храни, към
                                    които имате алергии, a ако нямате, то просто напишете "Не":</label>
                                <br>
                                <input value="{{old('allergic')}}" type="text" id="allergic"
                                       class="@error('allergic') is-invalid @enderror consult-other-input"
                                       name="allergic" autocomplete="allergic">
                                @error('allergic')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        @endif
                        <div class="consult-content-address">
                            <strong>За дата*</strong>
                            <br>
                            <label class="consult-other-label" for="start_date">Моля, посочете дата, за която искате да
                                са готови рецептите:</label>
                            <br>
                            <input value="{{old('start_date')}}" type="datetime-local" id="start_date"
                                   class="@error('start_date') is-invalid @enderror consult-other-input"
                                   name="start_date" autocomplete="start_date">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="consult-content-address">
                            <strong>Допълнителна информация*</strong>
                            <br>
                            <label class="consult-other-label" for="info">Моля, ако има нещо, което сме пропуснали във
                                формуляра си, опишете го тук:</label>
                            <br>
                            <textarea id="allergic" class="@error('info') is-invalid @enderror consult-other-textarea"
                                      name="info" autocomplete="info">{{old('info')}}</textarea>
                            @error('info')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mx-auto text-center">
                            <button class="consult-form-submit-button-{{$type}}">
                                Изпрати заявката!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="consult-show-template-image w-100">
            <img class="w-100" src="/storage/consult/show/background-up.png" alt="template">
        </div>
        <div class="consult-middle-images">
            <div class="row consult-row-wrapper">
                <div class="col-lg-2 consult-middle-images-{{$type}}">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-1.png"
                         alt="consult-1">
                </div>
                <div class="col-lg-2 consult-middle-images-even">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-2.png"
                         alt="consult-2">
                </div>
                <div class="col-lg-2 consult-middle-images-{{$type}}">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-3.png"
                         alt="consult-3">
                </div>
                <div class="col-lg-2 consult-middle-images-even">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-4.png"
                         alt="consult-4">
                </div>
                <div class="col-lg-2 consult-middle-images-{{$type}}">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-5.png"
                         alt="consult-5">
                </div>
                <div class="col-lg-2 consult-middle-images-even">
                    <img class="consult-middle-images-image" src="/storage/consult/show/consult-6.png"
                         alt="consult-6">
                </div>
            </div>
        </div>
        <div class="row px-5 mx-auto text-center my-5">
            <div class="col-md-5 text-center">
                <img class="w-75" src="/storage/consult/show/boev.png" alt="cheff-boev">
            </div>
            <div class="col-md-7">
                <div class="text-center font-weight-bold consult-font consult-font-size-6 mt-5">
                <span>
                    Шеф Димитър Боев
                </span>
                </div>
                <div class="text-left consult-font-size-2">
                    @if($consultType->description->description !== NULL)
                        @foreach(explode(PHP_EOL, $consultType->description->description) as $paragraph)
                            <p>
                                {{$paragraph}}
                            </p>
                        @endforeach
                    @endif
                    <div class="w-100 mx-auto text-center">
                        <img class="w-50" src="/storage/consult/show/business/chef-boev.png" alt="chef">
                    </div>
                </div>
            </div>
        </div>
        <div class="consult-recipes-services-bar-{{$type}}">
            <div class="consult-recipes-services-up-slider">
                @foreach($upRecipes as $up)
                    <div class="consult-recipes-services-recipe">
                        <a href="{{route('recipes-show', $up->slug)}}">
                            <img src="{{implode(',',$up->images()->get()->pluck('url')->toArray())}}" alt="">
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="consult-recipes-services-bar-information recipes-services-{{$type}}">
                <div class="desktop-consult-recipes-title w-100 consult-font-size-6 consult-font text-center">
                    <span>
                        @if($type == 'beginner')
                            За лични събития
                        @else
                            За бизнеса
                        @endif
                    </span>
                    <br>
                    <img src="/storage/consult/show/business/middle.png" alt="middle">
                </div>
                <div class="mobile-consult-recipes-title">
                    <span>
                        @if($type == 'beginner')
                            За лични събития
                        @else
                            За бизнеса
                        @endif
                    </span>
                    <br>
                    <img src="/storage/consult/show/business/middle.png" alt="middle">
                </div>
                <div
                    class="desktop-consult-recipes-services w-90 mx-auto text-center consult-font-size-3 font-weight-bold font-italic">
                    <div class="row w-100 mx-auto justify-content-center">
                        @foreach($services as $service)
                            <div class="col-md-5 mx-1 border-bottom consult-recipes-services-wrapper">
                                <div>
                                    * {{$service->service}}
                                </div>
                                <div class="consult-recipes-services-price mx-auto">
                                    @if($service->consultPrice)
                                        На цена от
                                        <strong>
                                            {{$service->consultPrice->price}} лв.
                                        </strong>
                                    @else
                                        Не е зададена все още!
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="w-100 text-center consult-font-size-0-5 my-5 border-bottom">
                        <div class="consult-font-size-2">
                            Попълнете формата за заявка и ние ще се свържа с Вас възможно най-скоро!
                        </div>
                    </div>
                    <div class="text-center mx-auto">
                        <a href="">

                        </a>
                        <button id="{{$type}}" class="consult-query-button-{{$type}}"
                                onclick="return showForm(this.id)">
                            Заявка!
                        </button>
                    </div>
                </div>
                <div class="mobile-consult-recipes-services row">
                    @foreach($services as $service)
                        <div class="col-md-5 mx-1 border-bottom consult-recipes-services-wrapper">
                            <div>
                                * {{$service->service}}
                            </div>
                            <div class="consult-recipes-services-price mx-auto">
                                @if($service->consultPrice)
                                    На цена от
                                    <strong>
                                        {{$service->consultPrice->price}} лв.
                                    </strong>
                                @else
                                    Не е зададена все още!
                                @endif
                            </div>
                        </div>
                    @endforeach
                    <div class="w-100 text-center consult-font-size-0-5 my-5 border-bottom">
                        Попълнете формата за заявка и ще се свържа с Вас възможно най-скоро!
                    </div>
                    <div class="text-center mx-auto">
                        <a href="">

                        </a>
                        <button id="{{$type}}" class="consult-query-button-{{$type}}"
                                onclick="return showForm(this.id)">
                            Заявка!
                        </button>
                    </div>
                </div>
            </div>
            <div class="consult-recipes-services-up-slider">
                @foreach($downRecipes as $down)
                    <div class="consult-recipes-services-recipe">
                        <a href="{{route('recipes-show', $down->slug)}}">
                            <img src="{{implode(',',$down->images()->get()->pluck('url')->toArray())}}" alt="">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="w-100 my-5 mx-auto">
            <div class="w-100 text-center my-2">
                <span class="consult-font consult-font-size-6 font-weight-bold">Отзиви и коментари</span>
                <br>
                <img class="w-40" src="/storage/consult/show/business/comments-above.png" alt="comments-above">
            </div>
            @foreach($comments as $comment)
                <div class="w-75 consult-font-size-2 my-5 mx-auto">
                    <div
                        class="consult-comments-user-{{$type}} @if($loop->iteration % 2 === 1) consult-comments-user-odd-{{$type}} @endif">
                        {{$comment->from}}
                    </div>
                    <div
                        class="consult-comments-body-{{$type}} @if($loop->iteration % 2 === 0) consult-comments-body-even-{{$type}} @endif">
                        <p>
                            {{$comment->message}}
                        </p>
                    </div>
                    <div
                        class="consult-comments-date-{{$type}} @if($loop->iteration % 2 === 1) consult-comments-date-odd-{{$type}} @endif">
                        {{$comment->created_at->format('H:i d-m-Y')}}
                    </div>
                </div>
            @endforeach
            <div class="w-100 d-flex justify-content-center consult-font-size-2">
                <div>
                    {{$comments->links()}}
                </div>
            </div>
        </div>
        <div class="w-100 my-5">
            <form action="{{route('consult-store-comment', $type)}}" method="post">
                @csrf
                @method('PATCH')
                <div class="w-75 my-5 mx-auto">
                    <div class="consult-comments-form">
                        <label for="sender"></label>
                        <input class="consult-input consult-input-{{$type}}" type="text" name="sender" id="sender"
                               placeholder="Име и фамилия...">
                    </div>
                    <div>
                        <label for="message">
                        </label>
                        <textarea placeholder="Отзив..." class="consult-textarea consult-textarea-{{$type}}"
                                  id="message" name="message"></textarea>
                    </div>
                    <div class="consult-comments-button">
                        <button class="consult-button-{{$type}}">Изпрати!</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        const bodyHeight = document.body.scrollHeight.toString() + 'px';
        let consultForm = document.getElementById('consult-form');

        let closeButton = document.getElementsByClassName('consult-form-close-button');

        function showForm(el) {
            let isShowed = true;
            let formWrapper = document.getElementsByClassName('consult-form-main-wrapper-' + el);
            formWrapper[0].style.height = bodyHeight;
            formWrapper[0].classList.add('show-form');
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;

            closeButton[0].addEventListener("click", () => {
                formWrapper[0].classList.remove('show-form');
            })

            formWrapper[0].addEventListener("click", () => {
                if (isShowed) {
                    isShowed = false;
                    formWrapper[0].children[0].addEventListener("click", () => {
                        isShowed = true;
                    });
                } else {
                    formWrapper[0].classList.remove('show-form');
                }
            })
            document.body.addEventListener('keyup', function (e) {
                if (e.key === "Escape") {
                    formWrapper[0].classList.remove('show-form');
                }
            });
        }
    </script>
@endsection
