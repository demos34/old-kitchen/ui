@extends('layouts.app')

@section('meta')
    <meta name="description" content="Старата кухня - консултации за Вашето меню!">
    <meta name="keywords"
          content="консултации">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Консултации за вашето меню:
    </title>
@endsection

@section('content')
    <div class="consult-wrapper">
        <div class="consult-paragraph-one">
            <p>
                Представям Ви новата услуга, която предлагам! Може да я използвате по всяко време, а аз Ви гарантирам успех!
            </p>
        </div>
        <div class="consult-paragraph-two">
            <p>
                Всеки може да се възползва от нея - без значение дали е за бизнес събиране или за личен повод! Аз съм тук заради вас!
            </p>
        </div>
        <div class="consult-paragraph-three">
            <p>
                Ще Ви помогна да осъществите вашите най-смели идеи и да се представите като блестящи както в избора, така и в подготовката на ястията, които ще представите!
            </p>
        </div>
        <div class="consult-paragraph-four">
            <p>
                Изберете тук какъв тип консултация Ви е необходим.
            </p>
        </div>
    </div>
    <div class="consult-middle-part">
        <div>
            <a href="{{route('consult-show', 'beginner')}}">
                <button class="consult-middle-part-first-button">
                    <img class="consult-middle-part-first-button-image" src="/storage/consult/arrow.svg" alt="right-arrow">
                    За лични
                </button>
            </a>
        </div>
        <div>
            <a href="{{route('consult-show', 'business')}}">
                <button class="consult-middle-part-second-button">
                    За бизнеси <img class="consult-middle-part-second-button-image" src="/storage/consult/arrow.svg" alt="right-arrow">
                </button>
            </a>
        </div>
    </div>
@endsection

