@extends('layouts.app')

@section('meta')
    <meta name="description" content="Старата кухня - свържи се с нас!">
    <meta name="keywords"
          content="контакти">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня | Контакти:
    </title>
@endsection

@section('content')
    <div class="w-100" id="book">
        <div class="mx-auto w-80 text-center">
            <div class="text-center my-3">
                <span class="send-us-msg-contacts">
                    За връзка с нас:
                </span>
            </div>
            <div class="book-show-wrapper text-center mx-auto">
                <div>
                    <a target="_blank" href="https://www.facebook.com/staratakuhnia">
                    <img class="contacts-images" src="/storage/home/fb.png" alt="fb">
                    </a>
                </div>
                <div>
                    <a target="_blank" href="https://www.instagram.com/staratakuhnia/?fbclid=IwAR0pMwOIZIat-NFy9tFlubAi6WpR8HTNIemowD-yPS7wVuV5X9vKbYC0WSA">
                    <img class="contacts-images" src="/storage/home/in.png" alt="fb">
                    </a>
                </div>
                <div>
                    <a target="_blank" href="https://www.youtube.com/channel/UCGQNduHc9DOfOY-vQtqWx7A?view_as=subscriber&fbclid=IwAR3aHIjCygG6qp3boOaSdp5j3h7pfNSaMwTBYSp8-mUsF9GHHZbFjqXwirQ">
                    <img class="contacts-images" src="/storage/home/tube.png" alt="fb">
                    </a>
                </div>
            </div>
            <div class="text-center mx-auto d-flex justify-content-center">
                <form action="{{route('contacts-post')}}" method="POST" class="text-center mx-auto">
                    @csrf
                    @method('PUT')
                    <div class="w-100 text-left my-2">
                        <label for="email" class="contacts-label">
                            Имейл:
                        </label>
                        <input id="email" type="email"
                               class="contacts-inputs form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="emails">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="w-100 text-left my-2">
                        <label for="msg" class="contacts-label">
                            Вашето съобщение:
                        </label>
                        <textarea id="msg"
                                  name="msg"
                                  type="text"
                                  class="contacts-inputs form-control @error('email') is-invalid @enderror"
                                  required autocomplete="msg">{{ old('msg') }}</textarea>
                        @error('msg')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="text-center w-100">
                        <button class="books-contacts-button">
                            Изпрати
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
