@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$homeInfo->description}}">
    <meta name="keywords"
          content="@foreach($tags as $tag) @if($lastTag->name == $tag->name){{$tag->name}}@else{{$tag->name}}, @endif @endforeach">
    <meta name="author" content="Димитър Боев">
@endsection

@section('title')
    <title>
        Старата кухня
    </title>
@endsection

@section('content')
    <div class="w-100">
        <img src="{{$homeInfo->top_image_url}}" class="top-image-url mx-auto" alt="top-image">
    </div>
    <div class="container text-center">
        <div class="container">
            <div class="about-me-wrapper">
                <div class="my-5 text-center about-me-div">
{{--                    <img class="about-me-image" src="/storage/home/about-me-new.png" alt="about-me">--}}
                    <span class="about-me-span">
                        За мен
                    </span>
                </div>
            </div>
            <div class="mx-auto">
                <div class="col-md-3">
                    <img class="middle-image-url" src="{{$homeInfo->middle_image_url}}" alt="middle-image-url">
                </div>
                <div class="middle-home-text">
                    <p>
                        {{$homeInfo->middle_home_text}}
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="last-recipes-title-wrapper">
                <div class="my-auto text-center last-recipes-title-div">
{{--                    <img class="last-recipes" src="/storage/home/last-recipes.png" alt="last-recipes">--}}
                    <span class="last-recipes-image-span">
                        Последни рецепти
                    </span>
                </div>
            </div>
            <div class="row my-5 mx-auto text-center">
                @foreach($recipes as $recipe)
                    <div class="col-md-4 mx-auto">
                        <div>
                            <a href="{{route('recipes-show', $recipe->slug)}}">
                                <img class="index-last-recipe-image" src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}" alt="index-last-recipe">
                            </a>
                        </div>
                        <div class="index-title">
                            <h1 class="font-weight-bold text-center">
                                {{Str::limit($recipe->title, 10)}}
                            </h1>
                        </div>
                        <div class="index-last-recipe-div text-center">
                            <p class="">
                                {{$recipe->how_to}}
                            </p>
                        </div>
                        <div class="my-5 text-center">
                            <p>
                                <a href="{{route('recipes-show', $recipe->slug)}}"
                                   class="font-weight-bold index-last-recipe-a">
                                    повече
                                </a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex justify-content-center my-5">
                <div class="text-center">
                    <a href="{{route('recipes')}}">
                        <button type="button" class="btn index-view-all-btn">
                            виж всички
                        </button>
                    </a>
                </div>
            </div>
        </div>
@endsection
